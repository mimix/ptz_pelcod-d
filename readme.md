
## 工程简介

一款基于STC89C52的摄像机云台(PTS-326)，基于PELCOD-D协议

在AIO-RK3588s-JD4 开发板中进行开发测试



## 工程目录

```bash
.
├── CMakeLists.txt
├── build                       # 编译目录
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   ├── Makefile
│   ├── cmake_install.cmake
│   └── run_tset                # 可执行文件
├── example                     # C++参考例程
│   ├── 001.cpp
│   └── pelco-d-cpp
├── image
├── main.cpp                    # 基于linux C++的控制程序
├── model                       # 基于linux C++的相关操作部件
│   ├── PelcoD.cpp
│   ├── PelcoD.h
│   ├── common.h
│   ├── ptz.cpp
│   └── ptz.h
├── pts-326_python.py           # 基于Python的在linux中的控制代码
├── pts-326_python_windown.py   # 基于Python在Windows中的控制代码
├── python-example              # Python参考例程
│   ├── demo001.py
│   ├── demo002.py
│   ├── demo003.py
│   ├── demo004.py
│   └── demo005.py
└── readme.md

7 directories, 20 files

```

## 系统设计

定义一个云台类，该类具有左移、右移的方法

ptz类

### PELCOD-D协议

#### 协议命令格式

| 字节 1| 字节 2| 字节 3| 字节 4| 字节 5|字节 6| 字节 7|
|-------|------|-------|-------|------|------|-------|
| 同步字节| 地址码| 指令码1| 指令码2| 数据码1| 数据码2| 校验码|

#### 同步字节

一般为0xff

#### 地址码

根据设备实际

#### 指令

|   |Bit 7|Bit 6|Bit 5|Bit 4|Bit 3|Bit 2|Bit 1|Bit 0|
|---|-----|-----|-----|-----|-----|-----|-----|-----|
|指令码1|Sence码|为0|为0|自动/手动扫描|摄像机打开/关闭|光圈关闭|光圈打开|焦距拉近|
|指令码2|焦距拉远|视角变宽|视角变窄|上|下|左|右|为0|

#### 数据


## 记录

2023年6月27日
1. 创建工程项目代码
2. 添加【https://github.com/arneboe/pelco-d-cpp】代码参考

2023年6月28日
1. 参考【https://blog.csdn.net/GUA8122HOU/article/details/120565812】，怀疑已经将89号预置点删除掉了


## 问题记录


### 出现写入数据失败的情况

```bash
root@fuxivision:~/app/locke/ptz_pelcod-d/build# ./run_tset
--- 云 台 控 制 程 序 启 动 ! ---
>>>Create PTZ instance
Failed to write data to serial port.
```

根据手册中的故障排查
1. 控制线极性接反
2. 协议错误
3. 通信波特率错误
4. 地址码错误
   
问题是控制线极性接错了
需要等待一下，指令没错，发现设备的485口出现问题，无法发出信息


### 设备485接口发送数据无反应

485设备号：/dev/ttyS3

可以打开设备，但是发送数据不过去……【确定是线接错了】





















