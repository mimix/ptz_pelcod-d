import serial
import time

# 定义COM端口和波特率
COM_PORT = 'COM13'
BAUD_RATE = 9600

# 初始化串口
ser = serial.Serial(COM_PORT, BAUD_RATE, timeout=1)

# 函数：设备左转
def turn_left():
    command = "PELCOD,TURN,LEFT\r\n"
    send_command(command)

# 函数：设备右转
def turn_right():
    command = "PELCOD,TURN,RIGHT\r\n"
    send_command(command)

# 函数：设置预置点
def set_preset(preset_number):
    command = f"PELCOD,PRESET,SET,{preset_number}\r\n"
    send_command(command)

# 函数：调用预置点
def call_preset(preset_number):
    command = f"PELCOD,PRESET,CALL,{preset_number}\r\n"
    send_command(command)

# 函数：删除预置点
def delete_preset(preset_number):
    command = f"PELCOD,PRESET,DELETE,{preset_number}\r\n"
    send_command(command)

# 函数：发送命令
def send_command(command):
    ser.write(command.encode())
    time.sleep(0.1)
    response = ser.readline().decode().strip()
    print(f"Received response: {response}")

# 主程序入口
if __name__ == '__main__':
    # 控制设备左转和右转
    turn_left()
    time.sleep(1)
    turn_right()
    time.sleep(1)

    # 设置、调用和删除预置点
    preset_number = 1
    set_preset(preset_number)
    call_preset(preset_number)
    delete_preset(preset_number)

    # 关闭串口连接
    ser.close()
