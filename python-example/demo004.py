import serial
import time
import keyboard

# 定义串口参数
COM_PORT = 'COM13'
BAUD_RATE = 9600
ser = None

# 打开串口连接
def open_serial():
    global ser
    ser = serial.Serial(COM_PORT, BAUD_RATE, timeout=1)
    print(f"Serial port {COM_PORT} opened.")

# 关闭串口连接
def close_serial():
    if ser:
        ser.close()
        print(f"Serial port {COM_PORT} closed.")

# 发送命令并接收响应
def send_command(command):
    ser.write(command.encode())
    response = ser.readline().decode().strip()
    print(f"Response: {response}")

# 设备左转
def turn_left():
    command = "PELCOD,TURN,LEFT\r\n"
    send_command(command)

# 设备右转
def turn_right():
    command = "PELCOD,TURN,RIGHT\r\n"
    send_command(command)

# 设置预置点
def set_preset(preset_number):
    command = f"PELCOD,PRESET,SET,{preset_number}\r\n"
    send_command(command)

# 调用预置点
def call_preset(preset_number):
    command = f"PELCOD,PRESET,CALL,{preset_number}\r\n"
    send_command(command)

# 删除预置点
def delete_preset(preset_number):
    command = f"PELCOD,PRESET,DELETE,{preset_number}\r\n"
    send_command(command)

# 键盘监听回调函数
def on_key_press(event):
    if event.name == 'left':
        turn_left()
    elif event.name == 'right':
        turn_right()

# 主程序入口
if __name__ == '__main__':
    # 打开串口连接
    open_serial()

    # 注册键盘监听回调函数
    keyboard.on_press(on_key_press)

    # 保持程序运行，直到按下Esc键退出
    keyboard.wait('esc')

    # 关闭串口连接
    close_serial()
