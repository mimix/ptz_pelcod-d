import serial

class PELCODInterface:
    def __init__(self, port, baudrate):
        self.ser = serial.Serial(port, baudrate)
    
    def send_command(self, data):
        self.ser.write(data)

    def move_left(self, speed=0x20):
        data = bytearray([0xFF, 0x01, 0x00, speed, 0x00, 0x03, (0x20 + speed) % 256])
        self.send_command(data)

    def move_right(self, speed=0x20):
        data = bytearray([0xFF, 0x01, 0x00, 0x00, speed, 0x03, (0x23 + speed) % 256])
        self.send_command(data)

    def set_preset(self, preset_num):
        data = bytearray([0xFF, 0x01, 0x00, 0x00, 0x00, (0x03 + preset_num) % 256, preset_num])
        self.send_command(data)

    def close(self):
        self.ser.close()

# 示例用法
if __name__ == "__main__":
    # 初始化串口
    interface = PELCODInterface('/dev/ttyUSB0', 9600)

    # 控制云台向左移动
    interface.move_left()

    # 控制云台向右移动
    interface.move_right()

    # 设置预置点
    interface.set_preset(1)

    # 关闭串口
    interface.close()
