def calculate_checksum(data):
    checksum = sum(data[1:6]) & 0xFF
    return checksum

data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

checksum = calculate_checksum(data)
print("校验和：", checksum)
