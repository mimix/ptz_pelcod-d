#include "ptz.h"
#include "common.h"

ptz::ptz(/* args */)
{
    serialParams = {0};
    PelcoD_data_len = 7;
    printf(">>>Create PTZ instance\n\r");
}

ptz::~ptz()
{
    clear();
    ptz_del(con_fd);
    printf(">>>Destroy PTZ instance\n\r");
}
//初始化实例
int ptz::ptz_init(std::string dev){

    //打开设备
    con_fd = open(dev.c_str(),O_RDWR | O_NOCTTY | O_NDELAY);//
    if (con_fd == -1)
    {
        // 处理打开失败的情况
        std::cerr << "Failed to open serial port." << std::endl;
        exit(1);
    }else{
        printf(">>> open serial port OK!!!\n\r");
    }

    //初始化串口配置
    if (tcgetattr(con_fd, &serialParams) != 0)
    {
        // 处理获取串口参数失败的情况
        std::cerr << "Failed to get serial port parameters." << std::endl;
        close(con_fd);
        exit(1);
    }else{
        printf(">>> get serial port parameters OK!!!\n\r");
    }

    //设置输如输出波特率：2400
    cfsetispeed(&serialParams, B2400); // 设置输入波特率
    cfsetospeed(&serialParams, B2400); // 设置输出波特率

    serialParams.c_cflag |= CS8;               // 设置数据位为8位
    serialParams.c_cflag &= ~PARENB;           // 禁用奇偶校验
    serialParams.c_cflag &= ~CSTOPB;           // 设置停止位为1位

    //设置串口配置
    if (tcsetattr(con_fd, TCSANOW, &serialParams) != 0)
    {
        // 处理设置串口参数失败的情况
        std::cerr << "Failed to set serial port parameters." << std::endl;
        close(con_fd);
        exit(1);
    }else{
        printf(">>> set serial port parameters OK!!!\n\r");
    }

    PelcoD_init(0x06);//初始化，给入地址

    return 0;
}
//销毁实例
int ptz::ptz_del(int fd){

    clear();
    close(fd);

    return 0;
}
//设置预置点
int ptz::set_preset(uint8_t preset){

    setPreset(preset);
    calcChecksum();
    sendData(con_fd,(char *)PelcoD_data,PelcoD_data_len);

    return 0;
}

int ptz::get_preset(uint8_t preset){

    getPreset(preset);
    calcChecksum();
    sendData(con_fd,(char *)PelcoD_data,PelcoD_data_len);

    return 0;
}


//向左移动
int ptz::move_left(uint8_t speed){

    //组数据
    clear();
    moveLeft(speed);
    calcChecksum();
    //发送
    sendData(con_fd,(char *)PelcoD_data,7);
    return 0;
}
//右移
int ptz::move_right(uint8_t speed){

    clear();
    moveRight(speed);
    calcChecksum();
    //发送
    sendData(con_fd,(char *)PelcoD_data,7);
    return 0;
}

int ptz::diy_command(uint8_t a2,uint8_t a3,uint8_t a4,uint8_t a5){

    clear();
    PelcoD_data[2] = a2;
	PelcoD_data[3] = a3;
	PelcoD_data[4] = a4;
	PelcoD_data[5] = a5;
    calcChecksum();
    sendData(con_fd,(char *)PelcoD_data,PelcoD_data_len);

    return 0;
}

int ptz::action_stop(){

    PelcoD_data[2] = 0;
	PelcoD_data[3] = 0;
	PelcoD_data[4] = 0;
	PelcoD_data[5] = 0;
    calcChecksum();
    sendData(con_fd,(char *)PelcoD_data,PelcoD_data_len);

    return 0;
}


int ptz::sendData(int fd, const char* data, int dataSize)
{
    ssize_t bytesWritten = write(fd, data, dataSize);
    if (bytesWritten == -1)
    {
        std::cerr << "Failed to write data to serial port." << std::endl;
        exit(9);
    }

    printf(">>> sendData is bytesWritten:%d ,0x%02x-0x%02x-0x%02x-0x%02x-0x%02x-0x%02x-0x%02x\n\r",bytesWritten,
                                    data[0],data[1],data[2],
                                    data[3],data[4],data[5],data[6]);

    return bytesWritten;
}





