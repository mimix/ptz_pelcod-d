#ifndef PTZ_H
#define PTZ_H

#include "common.h"
#include "PelcoD.h"

//类：云台
class ptz: public PelcoD
{
private:
    /* data */
    struct termios serialParams;//串口参数
    int PelcoD_data_len;//协议命令长度
    
public:

    int con_fd;// 串口标识符

    ptz(/* args */);
    ~ptz();

    int ptz_init(std::string dev);//初始化
    int ptz_del(int fd);//销毁实例

    int move_left(uint8_t speed);//左移
    int move_right(uint8_t speed);//右移
    int set_preset(uint8_t preset);//设置预置点
    int get_preset(uint8_t preset);//调用预置点
    int action_stop();//动作停止
    int diy_command(uint8_t a2,uint8_t a3,uint8_t a4,uint8_t a5);//自设计指令

    int sendData(int fd, const char* data, int dataSize);
};





#endif

