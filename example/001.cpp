/*
    接收键盘按键控制并执行相应的函数
    
*/
#include <iostream>
#include <unistd.h>
#include <termios.h>

// 定义函数A
void A() {
    std::cout << "执行函数A" << std::endl;
}

// 定义函数B
void B() {
    std::cout << "执行函数B" << std::endl;
}

// 定义停止函数
void stop() {
    std::cout << "执行停止函数" << std::endl;
    exit(0);
}

// 获取键盘按键的函数
int getch() {
    struct termios oldt, newt;
    int ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return ch;
}

int main() {
    int key;
    while (true) {
        key = getch();
        if (key == 27) { // 判断是否为方向键
            key = getch();
            if (key == 91) { // 判断是否为方向键后的[
                key = getch();
                if (key == 68) { // 左方向键
                    A();
                } else if (key == 67) { // 右方向键
                    B();
                }
            }
        } else if (key == 32) { // 空格键
            stop();
        }
    }
    return 0;
}
