/*
    项目：云台控制程序

*/

#include "ptz.h"
#include <csignal>

int fd_w;
void signal_handler(int signum)
{
    close(fd_w);

    std::cout << "Interrupt signal (" << signum << ") received.\n";

    exit(signum);
}

// 获取键盘按键的函数
int getch() {
    struct termios oldt, newt;
    int ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return ch;
}

int main(int argc,char** argv){
    int key;

    std::cout << "--- 云 台 控 制 程 序 启 动 ! ---" << std::endl;
    signal(SIGINT, signal_handler);

    ptz ptz_dev;//构造对象

    ptz_dev.ptz_init("/dev/ttyS3");//初始化设备
    fd_w = ptz_dev.con_fd;//用于关闭485总线,测试使用

    //设置89号预置点使云台停止水平扫动
    // ptz_dev.get_preset(0x89);


    while (true) {
        key = getch();
        if (key == 27) { // 判断是否为方向键
            key = getch();
            if (key == 91) { // 判断是否为方向键后的[
                key = getch();
                if (key == 68) { // 左方向键
                    ptz_dev.move_left(0x3f);
                } else if (key == 67) { // 右方向键
                    ptz_dev.move_right(0x3f);
                }
            }
        } else if (key == 32) { // 空格键
            ptz_dev.action_stop();
        }
    }

    return 0;
}


